#! /usr/local/bin/python
# -*- coding: utf-8 -*-

import psycopg2 as pg
import json
import cgi
import os
import sys

class blogobj:
    def init_param(self):
        self.type = os.environ['REQUEST_METHOD']
        self.query = cgi.FieldStorage()

def fetchdata():
    conn = pg.connect("dbname=memodb user=postgres")
    cur = conn.cursor()
    cur.execute("SELECT * FROM memo;")
    re = cur.fetchall()
    ren = cur.rowcount
    conn.commit()
    cur.close()
    conn.close()

    resultset = []
    while(ren > -1):
        result = {}
        result['id'] =  re[ren - 1][0]
        result['title'] =  unicode(re[ren - 1][1],'euc-jp')
        result['body'] =  unicode(re[ren - 1][2],'euc-jp')
        resultset.append(result)
        ren -= 1

    resultall = {}
    resultall['result'] = resultset
    print json.dumps(resultall)

def insertdata(ttl, msg):
    conn = pg.connect("dbname=memodb user=postgres")
    cur = conn.cursor()

    enc_title = unicode( ttl, 'utf-8' ).encode( 'euc-jp' )
    enc_str = unicode( msg, 'utf-8' ).encode( 'euc-jp' ) 
    cur.execute("INSERT INTO memo(title,body) VALUES (%s, %s)",(enc_title, enc_str));
    conn.commit()
    cur.close()
    conn.close()

    resultall={}
    resultall['status'] = 'OK'
    resultall['message'] = 'Success! Your data will be safely stored.'
    print json.dumps(resultall)

def main():
    obj = blogobj()
    obj.init_param()

    print "Content-Type: application/json; Charset: utf-8; \n\n"
    if obj.type == 'GET':
        fetchdata()
    if obj.type == 'POST':
        message_key = 'text'
        message_title = 'title'
        if message_key in obj.query and message_title in obj.query:
            message = obj.query[message_key].value
            title = obj.query[message_title].value
            insertdata(title, message)
        else:
            resultall={}
            resultall['status'] = 'NG'
            resultall['message'] = 'Submit Failed.'
            print json.dumps(resultall)

if __name__ == "__main__":
    main()
