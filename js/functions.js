function init_param(){
    var q = location.search;
    var qs = q.substr(1).split('&');
    var qdata = {};
    for(var j = 0; j < qs.length; j++){
        var items = qs[j].split('=');
        qdata[items[0]] = items[1];
    }
    return qdata;
}
