(function(){
    var qdata = init_param();
    function getArticle(data){
        var id = Number(qdata['id']);
        var idx = -1;
        if(!isNaN(id)){
            for(var i = 0; i < data.result.length; i++){
                if(data.result[i].id === id){
                    idx = i; 
                    break;
                }
            }
        }
        else{
            idx = 0;
        }

        if(idx == -1){
            $('#wrap').append('<p>' + '記事がありません。'  + '</p>');
        }
        else{
            $('#wrap').append('<div>' + data.result[idx].title + '</div>');
            $('#wrap').append('<p>' + data.result[idx].body  + '</p>');
            var links = '<div id="nplink">';
            if(idx != data.result.length - 1)
                links += '<a href="?id=' + data.result[idx+1].id + '">' + "次の記事" +'</a>';
            links += "&nbsp;";
            if(idx != 0)
                links += '<a href="?id=' + data.result[idx - 1].id + '">' + "前の記事" +'</a>';
            links += "</div>";
            $('body').append(links);
        }
    }
    jQuery.getJSON('/misc/cgi-bin/test.py', getArticle);
})()
